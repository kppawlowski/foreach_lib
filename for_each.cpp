#pragma once
#include<algorithm>
#include<functional>

namespace ForEach
{

template< class InputIt, class BinaryFunction > 
void walkPairNeighbour(InputIt p_begin, InputIt p_end, BinaryFunction p_func)
{
	adjacent_find(p_begin, p_end, [p_func](auto& p_first, auto& p_second){
		return p_func(p_first, p_second), false;
	});
}

template< class InputIt, class BinaryFunction >
InputIt walkPairBySeparateElement(InputIt p_begin, InputIt p_end, BinaryFunction p_func)
{
    auto l_it = p_begin;
    
    for (; std::next(l_it) < p_end; std::advance(l_it, 2))
    {
        p_func(*l_it, *(std::next(l_it)));
    }
    
    return l_it;
}

};
