#include "for_each.cpp"
#include<cstdlib>
#include<iostream>
#include<vector>

int main()
{
    
    auto func = [](const auto& p_a, const auto& p_b){ 
        std::cout << p_a << " " << p_b << "\n"; 
    };
    
	std::vector<int> v = {1,2,3,4,5};
	ForEach::walkPairNeighbour(v.begin(), v.end(), func);
    auto i = ForEach::walkPairBySeparateElement(v.begin(), v.end(), func);
    
    if (i < v.end())
    {
        std::cout << "Other element: " << *i << "\n";
    }
}
